# scrobbler-client

Example client showing how to use the Scrobbler API.

## Setup

```
pip3 install -r requirements.txt
python3 -m scrobbler
```

TODO: You can run the scrobbler as a user service with systemd or something,
but I don't know how to do it.
