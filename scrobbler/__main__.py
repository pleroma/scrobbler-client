import asyncio
import json
import logging
import os
import sys

from .logger import setup as setup_logger
from .client import PleromaClient
from .mpris import setup_mpris_player, debug_callback


def setup_client(loop):
    print('What instance URL do you want to use?')
    instance = input('URL> ')

    client = PleromaClient(instance, 'Scrobbler',
                           'https://git.pleroma.social/pleroma/scrobbler-client',
                           'read write follow')

    print('Registering, please wait.')
    loop.run_until_complete(client.register())

    client.authorize(loop)

    print('Registration complete.')

    return client


class Scrobbler:
    def __init__(self, config_path):
        self.loop = asyncio.get_event_loop()
        self.mpris = setup_mpris_player(self.mpris_notify)
        self.config_path = config_path
        self.load_from_config()

    def load_from_config(self):
        self.client = None

        if not os.access(self.config_path, os.R_OK):
            logging.info('Config %r does not exist or is not readable, '
                         'a new config will be created.', self.config_path)
            self.client = setup_client(self.loop)
            self.write_config()
            return

        config_data = None
        with open(self.config_path, 'r') as fd:
            config_data = json.load(fd)

        if not config_data:
            logging.info('Config %r failed to load.', self.config_path)
            exit(2)

        self.client = PleromaClient(config_data['base_uri'], config_data['client_name'],
                                    config_data['client_website'], config_data['scopes'],
                                    config_data['client_secret'], config_data['client_id'],
                                    config_data['client_token'])

        logging.info('Loaded configuration from %r.', self.config_path)

    def write_config(self):
        with open(self.config_path, 'w') as fd:
            json.dump(self.client.persist(), fd)

        logging.info('Configuration written to %r.', self.config_path)

    def mpris_notify(self, metadata):
        if not self.client:
            return

        debug_callback(metadata)
        asyncio.ensure_future(self.client.listen(metadata))

    def run(self):
        self.loop.run_forever()


def main():
    setup_logger()

    if len(sys.argv) < 2:
        print('usage: python3 -m scrobbler config.json')
        print('       config.json will be created if it does not exist')
        exit(1)

    Scrobbler(sys.argv[1]).run()


if __name__ == '__main__':
    main()
