import aiohttp
import asyncio
import logging
import urllib.parse


class PleromaClient:
    def __init__(self, uri, client_name, client_website, scopes, client_secret=None, client_id=None, client_token=None):
        self.base_uri = uri
        self.client_name = client_name
        self.client_website = client_website
        self.scopes = scopes
        self.client_secret = client_secret
        self.client_id = client_id
        self.client_token = client_token

    def uri(self, path):
        return urllib.parse.urljoin(self.base_uri, path)

    async def register(self):
        if self.client_id and self.client_secret:
            return

        url = self.uri('/api/v1/apps')

        params = {
            'client_name': self.client_name,
            'redirect_uris': 'urn:ietf:wg:oauth:2.0:oob',
            'scopes': self.scopes,
            'website': self.client_website
        }

        async with aiohttp.ClientSession() as session:
            async with session.post(url, data=params) as resp:
                data = await resp.json()
                logging.debug('Registration response: %r.', data)

                self.client_secret = data['client_secret']
                self.client_id = data['client_id']

    @property
    def registered(self):
        return self.client_secret and self.client_id

    @property
    def authorize_uri(self):
        base_uri = self.uri('/oauth/authorize')
        payload = {
            'client_id': self.client_id,
            'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
            'response_type': 'code',
            'scope': 'read write follow'
        }

        return ''.join([base_uri, '?', urllib.parse.urlencode(payload)])

    def authorize(self, loop):
        if not self.registered:
            return

        print('An authorization token is needed to proceed.')
        print('Open a browser and go to:')
        print('  ', self.authorize_uri)

        exchange_token = input('token> ')

        loop.run_until_complete(self.exchange_token(exchange_token))

    @property
    def authorized(self):
        return self.registered and self.client_token

    async def exchange_token(self, token):
        token_uri = self.uri('/oauth/token')
        payload = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
            'grant_type': 'authorization_code',
            'code': token
        }

        async with aiohttp.ClientSession() as session:
            async with session.post(token_uri, data=payload) as resp:
                data = await resp.json()
                self.client_token = data['access_token']

        logging.debug('Client token set to %r.', self.client_token)

    @property
    def headers(self):
        return {'Authorization': 'Bearer %s' % (self.client_token)}

    async def listen(self, metadata):
        np_uri = self.uri('/api/v1/pleroma/scrobble')

        if not self.authorized:
            return

        async with aiohttp.ClientSession() as session:
            async with session.post(np_uri, data=metadata, headers=self.headers) as resp:
                data = await resp.json()
                logging.debug('Scrobble response: %r.', data)

    def persist(self):
        return {
            'client_secret': self.client_secret,
            'client_id': self.client_id,
            'client_token': self.client_token,
            'client_name': self.client_name,
            'client_website': self.client_website,
            'scopes': self.scopes,
            'base_uri': self.base_uri
        }


def main():
    from .logger import setup as setup_logger

    setup_logger()

    client = PleromaClient('https://pleroma.dereferenced.org',
                           'Test', 'test.com', 'read write follow')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.register())
    client.authorize(loop)


if __name__ == '__main__':
    main()
