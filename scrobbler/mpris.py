import asyncio
import logging

from dbus_next.aio import MessageBus
from dbus_next import Variant


async def discover_player(bus):
    dbus_introspection = await bus.introspect('org.freedesktop.DBus',
                                              '/org/freedesktop/DBus')

    dbus_proxy = bus.get_proxy_object('org.freedesktop.DBus',
                                      '/org/freedesktop/DBus',
                                      dbus_introspection)

    dbus_iface = dbus_proxy.get_interface('org.freedesktop.DBus')

    bus_names = await dbus_iface.call_list_names()
    bus_names = [name for name in bus_names if name.startswith('org.mpris.MediaPlayer2')]

    if not bus_names:
        return None

    return bus_names[0]


async def get_mpris_player():
    bus = await MessageBus().connect()

    bus_name = await discover_player(bus)
    if not bus_name:
        return

    logging.info('Discovered bus name: %r.', bus_name)

    mpris_introspection = await bus.introspect(bus_name, '/org/mpris/MediaPlayer2')
    mpris_proxy = bus.get_proxy_object(bus_name, '/org/mpris/MediaPlayer2',
                                       mpris_introspection)

    return mpris_proxy


def unpack_metadata(metadata):
    raw_data = metadata.value
    transient_data = {k: raw_data[k].value for k in raw_data.keys()}

    final_data = {
        'title': transient_data['xesam:title'],
        'album': transient_data['xesam:album'],
        'artist': ', '.join(transient_data['xesam:artist']),
        'length': int(transient_data['mpris:length'] / 1000)
    }

    return final_data


def debug_callback(finalized_metadata):
    logging.info('Now playing %r by %r.', finalized_metadata['title'],
                 finalized_metadata['artist'])


def setup_mpris_player(callback, loop=None):
    def changed_notify(interface_name, props, _):
        nonlocal callback

        if interface_name != 'org.mpris.MediaPlayer2.Player':
            return

        if 'Metadata' not in props:
            return

        metadata = unpack_metadata(props['Metadata'])

        callback(metadata)

    if not loop:
        loop = asyncio.get_event_loop()

    mpris_proxy = loop.run_until_complete(get_mpris_player())
    mpris_iface = mpris_proxy.get_interface('org.freedesktop.DBus.Properties')
    mpris_iface.on_properties_changed(changed_notify)

    logging.info('Ready.')

    return mpris_iface


def main():
    from .logger import setup as setup_logger

    setup_logger()

    loop = asyncio.get_event_loop()
    setup_mpris_player(debug_callback, loop)
    loop.run_forever()


if __name__ == '__main__':
    main()
